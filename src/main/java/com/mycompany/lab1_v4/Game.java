/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1_v4;

import java.util.Scanner;

/**
 *
 * @author Windows10
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }
    
    public void newGame(){
        this.table = new Table(player1, player2);
    }
    
    public void player() {
        showWelcome();
        newGame();
        while (true){
            showTable();
            showTurn();
            inputRowCol();
            
            if(table.checkWin()){
                showTable();
//                saveWin();
                showInfo();
                newGame();
            }
            if(table.checkDraw()){
                showTable();
//                saveDraw();
                showInfo();
                newGame();
            }
            table.switchPlayer();
        }
    }
    
//    private void saveWin(){
//        if(player1 == table.getCurrentPlayer()){
//            player1.win();
//            player2.lose();
//        }else{
//            player2.win();
//            player1.lose();
//        }
//    }
//    
//    private void saveDraw(){
//        player1.draw();
//        player2.draw();
//    }
    
    private void showWelcome() {
        System.out.println("Welcome to XO RowCol Games!");
    }

    private void showTable() {
       char[][] t = table.getTable();
       for(int row=0; row<3; row++){
           for(int col=0; col<3; col++){
               System.out.print(t[row][col]+" ");
           }
           System.out.println("");
       }
    }

    private void showTurn() {
        System.out.println("Turn: "+ table.getCurrentPlayer().getSymbol());
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input RowCol: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        
        if (table.checkRowCol(row, col)) {
            System.out.println("This point has already been chosen by the player");
            showTable();
            showTurn();
            System.out.println("Please input RowCol again");
            inputRowCol();
        }
        table.setRowCol(row, col);
        
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);

    }

}
