/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1_v4;

/**
 *
 * @author Windows10
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int countTurn = 0;
    private int row, col;
    public Table(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }        

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    
    public boolean checkRowCol(int row, int col){
        if(table[row][col] == 'X' || table[row][col] == 'O'){
            return true;
        }
        return false;
    }
    
    
    public boolean setRowCol(int row, int col){
        if(table[row][col] == '-'){
            table[row][col] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }
    
    public void switchPlayer(){
        if(currentPlayer ==  player1){
            currentPlayer = player2;
        }else{
            currentPlayer = player1;
        }
        
    }
    
    
    boolean checkWin() {
        if(checkRow()|| checkCol()|| checkXL() || checkXR()){
            saveWin();
            return true;
        }
        return false;
    }
    
    private boolean checkRow(){
        return table[row][0] !='-' && table[row][0] == table[row][1] && table[row][2]== table[row][1];
    }
    private boolean checkCol(){
        return table[0][col] !='-' && table[0][col] == table[1][col] && table[2][col]== table[1][col];
    }
    private boolean checkXL(){
        return table[0][0] !='-' && table[0][0] == table[1][1] && table[2][2] == table[1][1];
    }
    private boolean checkXR(){
        return table[0][2] !='-' && table[0][2] == table[1][1] && table[2][0] == table[1][1];
    }
    boolean checkDraw() {
        if(countTurn == 9){
            saveDraw();
            return true;
        }
        return false;
    }
    
    private void saveWin(){
        if(player1 == getCurrentPlayer()){
            player1.win();
            player2.lose();
        }else{
            player2.win();
            player1.lose();
        }
    }
    
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }
    
    
    
}
