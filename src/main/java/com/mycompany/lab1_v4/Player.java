/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1_v4;

/**
 *
 * @author Windows10
 */
public class Player {
    private char symbol;
    private int winCount , loseCount,drawCount;

    public int getWinCount() {
        return winCount;
    }
    
    public int getLoseCount() {
        return loseCount;
    }
    
    public int getDrawCount() {
        return drawCount;
    }

    public void win(){
        winCount++;
    }
    public void lose(){
        loseCount++;
    }
    public void draw(){
        drawCount++;
    }
  
    public Player(char symbol) {
        this.symbol = symbol; 
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount + '}';
    }
    
}
